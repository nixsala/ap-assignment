
package logandjdbc.ni.lk;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import net.proteanit.sql.DbUtils;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField txtuser;
	private JPasswordField txtpass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login log = new Login();
					log.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	
	
	
	public Login() {
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 600, 450, 700);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 102, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Welcome");
		lblNewLabel.setForeground(new Color(255, 204, 0));
		lblNewLabel.setFont(new Font("Abadi MT Condensed", Font.BOLD, 24));
		lblNewLabel.setBackground(new Color(240, 255, 240));
		lblNewLabel.setBounds(150, 11, 142, 31);
		contentPane.add(lblNewLabel);

		JLabel lbluser = new JLabel("User ID");
		lbluser.setFont(new Font("Abadi MT Condensed", Font.BOLD, 15));
		lbluser.setBounds(24, 57, 89, 31);
		contentPane.add(lbluser);

		JLabel lblpass = new JLabel("password");
		lblpass.setFont(new Font("Abadi MT Condensed", Font.BOLD, 15));
		lblpass.setBounds(24, 142, 212, 60);
		contentPane.add(lblpass);

		txtuser = new JTextField();
		txtuser.setBounds(24, 95, 244, 47);
		contentPane.add(txtuser);
		txtuser.setColumns(10);

		txtpass = new JPasswordField();
		txtpass.setBounds(24, 201, 244, 47);
		contentPane.add(txtpass);

		JButton butlog = new JButton("login");
		butlog.setBackground(new Color(248, 248, 255));
		butlog.setFont(new Font("Abadi MT Condensed", Font.BOLD, 15));
		butlog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// getting mysql connection
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					// write the query to select the particular table.
					String query = "SELECT * FROM login where customer_id= ? and  customer_password= ? ";
					
					PreparedStatement ps = con.prepareStatement(query);

					// retrive data from my sql and inserted in text box
					ps.setString(1, txtuser.getText());
					ps.setString(2, String.valueOf(txtpass.getPassword()));
					ResultSet rs = ps.executeQuery();

					// continous to check all fields.
					if (rs.next()) {

						// show message for login success and view the next jframe.
						JOptionPane.showMessageDialog(null, " Login Successfuly");
						InsideBook jframe = new InsideBook();
						jframe.setVisible(true);

						
					
											} else {
						// show error message and refresh the userid , password textboxes.
						JOptionPane.showMessageDialog(null, " Login Failed , Try Again");
						txtuser.setText(null);
						txtpass.setText(null);
					}

				} catch (Exception e2) {

					System.out.println(e2);

				}

			}

		});
		butlog.setBounds(10, 596, 89, 23);
		contentPane.add(butlog);

		JButton butcancel = new JButton("cancel");
		butcancel.setBackground(new Color(248, 248, 255));
		butcancel.setFont(new Font("Abadi MT Condensed", Font.BOLD, 15));
		butcancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		butcancel.setBounds(318, 596, 89, 23);
		contentPane.add(butcancel);

		JButton butsign = new JButton("Sign up");
		butsign.setBackground(new Color(248, 248, 255));
		butsign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Register jframe = new Register();
				jframe.setVisible(true);
			}
		});
		butsign.setFont(new Font("Abadi MT Condensed", Font.BOLD, 15));
		butsign.setBounds(109, 596, 89, 23);
		contentPane.add(butsign);

		JCheckBox chkagree = new JCheckBox("show password");
		chkagree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (chkagree.isSelected()) {
					txtpass.setEchoChar((char) 0);
				} else {
					txtpass.setEchoChar('*');
				}
			}

		});
		chkagree.setBounds(124, 525, 304, 31);
		contentPane.add(chkagree);

		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("G:\\Photoes\\login_07.gif"));
		lblNewLabel_1.setBounds(81, 276, 287, 223);
		contentPane.add(lblNewLabel_1);
		
		JButton butrefresh = new JButton("Refresh");
		butrefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtuser.setText(null);
				txtpass.setText(null);
				
			}
		});
		butrefresh.setFont(new Font("Abadi MT Condensed", Font.BOLD, 15));
		butrefresh.setBounds(219, 597, 89, 23);
		contentPane.add(butrefresh);
	}
}
