package logandjdbc.ni.lk;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;

import java.awt.Color;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;

public class CustomerPurchase extends JFrame {

	private JPanel contentPane;
	private JTextField txtbookid;
	private JLabel lblNewLabel;
	private JScrollPane scrollPane;
	private JButton butadd;
	public JTextField txttotal;
	private JTable table;
	public JTextField txtprice;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerPurchase frame = new CustomerPurchase();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection con = null;
	private JList listprice;
	private JButton butcalculate;
	private JScrollPane scrollPane_2;
	private JButton Calculate;
	private JTable tbbill;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;

	public CustomerPurchase() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root", "root");
		} catch (Exception e2) {

			// System.out.println(e2);
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 498);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 127, 177, 268);
		contentPane.add(scrollPane);

		JList listbid = new JList();
		scrollPane.setViewportView(listbid);
		listbid.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtbookid.setText(listbid.getSelectedValue().toString());
			}
		});

		txtbookid = new JTextField();
		txtbookid.setBounds(10, 63, 263, 46);
		contentPane.add(txtbookid);
		txtbookid.setColumns(10);

		JButton btnNewButton = new JButton("Start Purchase");
		btnNewButton.setForeground(new Color(255, 215, 0));
		btnNewButton.setBackground(new Color(0, 100, 0));
		btnNewButton.setFont(new Font("AabcedXBold", Font.BOLD, 16));

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultListModel model = new DefaultListModel();
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					Statement s = con.createStatement();
					String sql = "SELECT book_id,book_title,price FROM book_details  ";
					ResultSet rs = s.executeQuery(sql);
					int row = 0;
					while (rs.next())

						model.addElement(rs.getObject(1).toString() + "-" + rs.getObject(2).toString()
								+ " of the price is Rs " + rs.getObject(3).toString());

					System.out.println(model);
					listbid.setModel(model);
					rs.close();

				} catch (Exception e1) {

					e1.printStackTrace();

				}
			}

		});
		btnNewButton.setBounds(341, 0, 203, 57);
		contentPane.add(btnNewButton);

		lblNewLabel = new JLabel("Purchase");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Beware", Font.BOLD, 17));
		lblNewLabel.setBounds(10, 0, 136, 46);
		contentPane.add(lblNewLabel);

		butadd = new JButton("New button");
		butadd.setIcon(new ImageIcon("G:\\Photoes\\53609172-stock-vector-add-button.jpg"));
		butadd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.addRow(new Object[] { txtbookid.getText() });
				double total = 0;
				for (int i = 0; i < table.getRowCount(); i++) {
					double price = Double.parseDouble((String) table.getValueAt(i, 0));
					total += price;
				}
				txttotal.setText(String.valueOf(total));

				/*
				 * } catch (Exception e2) { System.out.println(e2); }
				 */

			}

		});
		butadd.setBounds(206, 368, 328, 64);
		contentPane.add(butadd);

		JLabel lblNewLabel_1 = new JLabel("Total :");
		lblNewLabel_1.setFont(new Font("Cambria Math", Font.BOLD, 17));
		lblNewLabel_1.setBounds(632, 401, 68, 26);
		contentPane.add(lblNewLabel_1);

		txttotal = new JTextField();
		txttotal.setBounds(738, 405, 86, 20);
		contentPane.add(txttotal);
		txttotal.setColumns(10);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(225, 145, 309, 193);
		contentPane.add(scrollPane_1);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "book details" }));
		scrollPane_1.setViewportView(table);

		txtprice = new JTextField();
		txtprice.setBounds(590, 95, 141, 26);
		contentPane.add(txtprice);
		txtprice.setColumns(10);

		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(574, 151, 177, 244);
		contentPane.add(scrollPane_2);

		listprice = new JList();
		scrollPane_2.setViewportView(listprice);
		listprice.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtprice.setText(listprice.getSelectedValue().toString());

			}
		});

		butcalculate = new JButton("Bill info");
		butcalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultListModel model = new DefaultListModel();
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					Statement s = con.createStatement();
					String sql = "SELECT price FROM book_details ";
					ResultSet rs = s.executeQuery(sql);
					int row = 0;
					while (rs.next())
						// model.addElement(rs.getString("book_title"));
						// model.addElement(rs.getObject(1).toString() +"-"+
						// rs.getObject(2).toString());
						model.addElement(rs.getObject(1).toString());

					System.out.println(model);
					listprice.setModel(model);
					rs.close();

				} catch (Exception e1) {

					e1.printStackTrace();

				}
			}

		});
		butcalculate.setBounds(761, 226, 126, 57);
		contentPane.add(butcalculate);

		Calculate = new JButton("Calculate");
		Calculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) tbbill.getModel();
				model.addRow(new Object[] { txtprice.getText() });
				double total = 0;
				for (int i = 0; i < tbbill.getRowCount(); i++) {
					double price = Double.parseDouble((String) tbbill.getValueAt(i, 0));
					total += price;
				}
				txttotal.setText(String.valueOf(total));

			}
		});
		Calculate.setBounds(761, 170, 126, 57);
		contentPane.add(Calculate);

		tbbill = new JTable();
		tbbill.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "price" }) {
			Class[] columnTypes = new Class[] { Double.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		tbbill.setBounds(842, 247, 2, 5);
		contentPane.add(tbbill);

		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon("G:\\Photoes\\download (4).jpg"));
		lblNewLabel_2.setBounds(923, 109, 251, 303);
		contentPane.add(lblNewLabel_2);

		btnNewButton_1 = new JButton("Next");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String print = txtprice.getText();
				CustomerBill jframe = new CustomerBill();
				jframe.setVisible(true);
			}
		});
		btnNewButton_1.setBackground(new Color(50, 205, 50));
		btnNewButton_1.setBounds(812, 0, 164, 26);
		contentPane.add(btnNewButton_1);

		btnNewButton_2 = new JButton("Exit");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton_2.setBackground(Color.RED);
		btnNewButton_2.setBounds(977, 0, 197, 26);
		contentPane.add(btnNewButton_2);
	}

	public JTextField getTxttotal() {
		return txttotal;
	}

	public JPanel getContentPane() {
		return contentPane;
	}
}
