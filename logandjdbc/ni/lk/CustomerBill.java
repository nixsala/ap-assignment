package logandjdbc.ni.lk;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.jdom.Document;

import net.proteanit.sql.DbUtils;

import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileOutputStream;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;

public class CustomerBill extends JFrame {

	private JPanel contentPane;
	private JList listbid;
	private JTable table;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerBill frame = new CustomerBill();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CustomerBill() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 657, 469);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("Logout");
		btnNewButton.setFont(new Font("TabAvarangal2", Font.BOLD, 18));
		btnNewButton.setForeground(new Color(218, 165, 32));
		btnNewButton.setBackground(new Color(0, 100, 0));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog( null, "thank yoy, purchase complete, come again");
				MainLogin jframe = new MainLogin();
		          jframe.setVisible(true);

			}

		});
		btnNewButton.setBounds(0, 0, 221, 20);
		contentPane.add(btnNewButton);

		JLabel lblNewLabel = new JLabel("Book store Billing");
		lblNewLabel.setFont(new Font("November", Font.BOLD, 23));
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setBounds(81, 38, 239, 49);
		contentPane.add(lblNewLabel);

		JButton butAdd = new JButton("Exit");
		butAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		butAdd.setForeground(new Color(255, 255, 255));
		butAdd.setBackground(new Color(95, 158, 160));
		butAdd.setFont(new Font("TabAvarangal2", Font.BOLD, 16));
		butAdd.setBounds(221, 1, 136, 21);
		contentPane.add(butAdd);

		JButton btnNewButton_1 = new JButton("print");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MessageFormat header = new MessageFormat("*******************book store bill****************");
				MessageFormat footer = new MessageFormat("*************bill print*******************");
				try {

					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root", "root");
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					

					table.print(JTable.PrintMode.NORMAL, header, footer);

				} catch (Exception e2) {
					System.out.println(e2);
				}

			}

		});
		btnNewButton_1.setBounds(353, 1, 101, 19);
		contentPane.add(btnNewButton_1);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"List of items"
			}
		));
		table.setBounds(324, 132, 307, 225);
		contentPane.add(table);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 213, 186, 178);
		contentPane.add(scrollPane);
		
		JList listbill = new JList();
		scrollPane.setViewportView(listbill);
		listbill.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				textField.setText(listbill.getSelectedValue().toString());
				

				
			}
			
		});
		
		JButton btnNewButton_2 = new JButton("Select purchased items");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultListModel model = new DefaultListModel();
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					Statement s = con.createStatement();
					String sql = "SELECT book_id,book_title,price FROM book_details  ";
					ResultSet rs = s.executeQuery(sql);
					int row = 0;
					while (rs.next())
						// model.addElement(rs.getString("book_title"));
						// model.addElement(rs.getObject(1).toString() +"-"+
						// rs.getObject(2).toString());
						model.addElement( rs.getObject(2).toString()
								+ " of the price is Rs " + rs.getObject(3).toString());

					System.out.println(model);
					listbill.setModel(model);
					rs.close();
					

				} catch (Exception e1) {

					e1.printStackTrace();

				}
			}
			
		});
		btnNewButton_2.setBounds(20, 132, 170, 49);
		contentPane.add(btnNewButton_2);
		
		textField = new JTextField();
		textField.setBounds(211, 141, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton butfinal = new JButton("Add to table");
		butfinal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.addRow(new Object[] { textField.getText() });
			}
		});
		butfinal.setBounds(374, 381, 122, 23);
		contentPane.add(butfinal);

	}

	public JPanel getContentPane() {
		return contentPane;
	}
}