package logandjdbc.ni.lk;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Frame;

public class MainLogin extends JFrame {

	private JPanel contentPane;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainLogin frame = new MainLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(105, 105, 105));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Main Login");
		lblNewLabel.setIcon(null);
		lblNewLabel.setForeground(new Color(255, 215, 0));
		lblNewLabel.setFont(new Font("TABThunaivan", Font.BOLD, 18));
		lblNewLabel.setBounds(153, 11, 115, 30);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("User Catogory");
		lblNewLabel_1.setFont(new Font("Harding", Font.BOLD, 14));
		lblNewLabel_1.setForeground(new Color(51, 204, 153));
		lblNewLabel_1.setBounds(36, 83, 118, 36);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Password");
		lblNewLabel_2.setFont(new Font("Harding", Font.BOLD, 14));
		lblNewLabel_2.setForeground(new Color(0, 204, 153));
		lblNewLabel_2.setBounds(46, 139, 92, 14);
		contentPane.add(lblNewLabel_2);

		passwordField = new JPasswordField();
		passwordField.setBounds(153, 137, 86, 17);
		contentPane.add(passwordField);
		String[] user_catogory = { "admin", "user" };
		JComboBox comboBox = new JComboBox(user_catogory);
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		comboBox.setBounds(164, 93, 75, 17);
		contentPane.add(comboBox);

		JButton butbuy = new JButton("Let's Start");
		butbuy.setBackground(new Color(240, 240, 240));
		butbuy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String user_catogory = comboBox.getSelectedItem().toString();
				String pword = passwordField.getText();

				if (pword.contains("admin123") && user_catogory.contains("admin")) {

					InsideBook iframe = new InsideBook();
					iframe.setVisible(true);

				} else {
					if (pword.contains("customer123") && user_catogory.contains("user")) {

						Login lframe = new Login();
						lframe.setVisible(true);

					} else {
						JOptionPane.showMessageDialog(null, "Login failed");
						passwordField.setText(null);
						// comboBox.getSelectedItem().toString(0);
						// JOptionPane.showMessageDialog(null, "Login failed");
						// passwordField.setText(null);

					}
				}
			}

		});
		butbuy.setBounds(10, 220, 99, 30);
		contentPane.add(butbuy);

		JButton butcancel = new JButton("Cancel");
		butcancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}

		});
		butcancel.setBounds(153, 220, 99, 30);
		contentPane.add(butcancel);

		JButton btnNewButton_1 = new JButton("Refresh");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				passwordField.setText(null);

			}
		});
		btnNewButton_1.setBounds(282, 224, 99, 26);
		contentPane.add(btnNewButton_1);

		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBackground(new Color(233, 150, 122));
		lblNewLabel_3.setForeground(new Color(105, 105, 105));
		lblNewLabel_3.setIcon(new ImageIcon("G:\\Photoes\\laptop_cup_glasses_plant_114948_300x168.jpg"));
		lblNewLabel_3.setBounds(22, 11, 499, 239);
		contentPane.add(lblNewLabel_3);
	}
}
