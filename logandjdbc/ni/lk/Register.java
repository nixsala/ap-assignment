package logandjdbc.ni.lk;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class Register extends JFrame {

	private JPanel contentPane;
	private JTextField txtname;
	private JTextField txtid;
	private JPasswordField txtpass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register frame = new Register();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Register() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(128, 0, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblsignup = new JLabel("Sign Up");
		lblsignup.setForeground(new Color(240, 230, 140));
		lblsignup.setFont(new Font("Carolus", Font.BOLD, 33));
		lblsignup.setBounds(126, 11, 213, 32);
		contentPane.add(lblsignup);

		JLabel lblname = new JLabel("Name");
		lblname.setBackground(new Color(169, 169, 169));
		lblname.setForeground(new Color(230, 230, 250));
		lblname.setFont(new Font("Abadi MT Condensed", Font.BOLD, 17));
		lblname.setBounds(10, 55, 59, 19);
		contentPane.add(lblname);

		txtname = new JTextField();
		txtname.setBounds(102, 54, 86, 20);
		contentPane.add(txtname);
		txtname.setColumns(10);

		JLabel lbl_id = new JLabel("User ID");
		lbl_id.setForeground(new Color(230, 230, 250));
		lbl_id.setBackground(new Color(230, 230, 250));
		lbl_id.setFont(new Font("Abadi MT Condensed", Font.BOLD, 17));
		lbl_id.setBounds(10, 95, 70, 17);
		contentPane.add(lbl_id);

		JLabel lblpass = new JLabel("Password");
		lblpass.setForeground(new Color(230, 230, 250));
		lblpass.setFont(new Font("Abadi MT Condensed", Font.BOLD, 17));
		lblpass.setBackground(new Color(230, 230, 250));
		lblpass.setBounds(-1, 138, 70, 17);
		contentPane.add(lblpass);

		txtid = new JTextField();
		txtid.setBounds(102, 94, 86, 20);
		contentPane.add(txtid);
		txtid.setColumns(10);

		JButton butsign = new JButton("Sign Up");
		butsign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// To connect the mysql database
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					// creating the object stmt
					Statement stmt = con.createStatement();

					// declare the varible in the text box
					String customer_id = txtid.getText();
					String customer_name = txtname.getText();
					String customer_password = txtpass.getText();

					// database insert command for the particular table login
					String insert = " INSERT INTO login values('" + customer_id + "','" + customer_name + "','"
							+ customer_password + "')";
					stmt.executeUpdate(insert);

					// popup the message if the user detail is successfuly inserted
					JOptionPane.showMessageDialog(null, "sign up successfully, continue login.........");
					Login iframe = new Login();
					iframe.setVisible(true);
					// handling exception
				} catch (Exception e2) {

				}

			}

		});
		butsign.setBounds(10, 227, 79, 23);
		contentPane.add(butsign);

		JButton butcancel = new JButton("Cancel");
		butcancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		butcancel.setBounds(116, 227, 86, 23);
		contentPane.add(butcancel);

		txtpass = new JPasswordField();
		txtpass.setBounds(102, 137, 86, 18);
		contentPane.add(txtpass);

		JButton btnConform = new JButton("Conform");
		btnConform.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login iframe = new Login();
				iframe.setVisible(true);
			}
		});
		btnConform.setBounds(227, 227, 89, 23);
		contentPane.add(btnConform);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("G:\\Photoes\\images (2).jpg"));
		lblNewLabel.setBounds(227, 54, 186, 162);
		contentPane.add(lblNewLabel);

		JButton btnNewButton = new JButton("Refresh");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtid.setText(null);
				txtname.setText(null);
				txtpass.setText(null);
			}
		});
		btnNewButton.setBounds(324, 227, 89, 23);
		contentPane.add(btnNewButton);
	}
}
