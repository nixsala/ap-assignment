package logandjdbc.ni.lk;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JScrollPane;

public class ShoppingCart extends JFrame {

	private JPanel contentPane;
	private JTextField txtshopid;
	private JTextField txtuserid;
	private JTextField txtpquantity;
	private JTextField txtbookid;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShoppingCart frame = new ShoppingCart();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ShoppingCart() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Shopping Cart");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel.setBounds(169, 11, 160, 20);
		contentPane.add(lblNewLabel);
		
		JLabel lblshopid = new JLabel("shopping id");
		lblshopid.setBounds(33, 45, 80, 17);
		contentPane.add(lblshopid);
		
		JLabel lbluserid = new JLabel("user id");
		lbluserid.setBounds(33, 94, 61, 17);
		contentPane.add(lbluserid);
		
		JLabel lblbookid = new JLabel("book id");
		lblbookid.setBounds(33, 131, 46, 14);
		contentPane.add(lblbookid);
		
		JLabel lblquantity = new JLabel("no of quantity");
		lblquantity.setBounds(10, 176, 80, 14);
		contentPane.add(lblquantity);
		
		txtshopid = new JTextField();
		txtshopid.setBounds(281, 43, 86, 20);
		contentPane.add(txtshopid);
		txtshopid.setColumns(10);
		
		txtuserid = new JTextField();
		txtuserid.setColumns(10);
		txtuserid.setBounds(281, 92, 86, 20);
		contentPane.add(txtuserid);
		
		txtpquantity = new JTextField();
		txtpquantity.setColumns(10);
		txtpquantity.setBounds(151, 173, 104, 20);
		contentPane.add(txtpquantity);
		
		txtbookid = new JTextField();
		txtbookid.setColumns(10);
		txtbookid.setBounds(281, 128, 86, 20);
		contentPane.add(txtbookid);
		
		JButton butconform = new JButton("Next");
		butconform.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerPurchase jframe = new CustomerPurchase();
		          jframe.setVisible(true);
			}
			
		});
		butconform.setBounds(126, 332, 89, 23);
		contentPane.add(butconform);
		
		JButton butback = new JButton("Back");
		butback.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerSelectBook jframe = new CustomerSelectBook();
		          jframe.setVisible(true);
			}
		});
		butback.setBounds(126, 262, 89, 23);
		contentPane.add(butback);
		
		JButton btnAddCart = new JButton("Add cart");
		btnAddCart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root", "root");	
					
					
					Statement stmt = con.createStatement();
					String shopping_id=txtshopid.getText();
					String user_id=txtuserid.getText();
					String book_id= txtbookid.getText();
					//SimpleDateFormat d = new SimpleDateFormat("dd-MM-yyyy");
			      //  String date = DATE_FORMAT.format(date);
					//JFormattedTextField tdate=new JFormattedTextField(new SimpleDateFormat("dd/MM/yyyy"));
					
					
					
					String quantity_of_purchase=txtpquantity.getText();
				
					
					
					
					String insert =" INSERT INTO shopping_cart values('"+shopping_id+"','"+book_id +"','"+user_id+"','"+quantity_of_purchase +"')";
					stmt.executeUpdate(insert);
		
					JOptionPane.showMessageDialog(null, " Added to this shopping cart");
					
				} catch (Exception e2) {
					System.out.println(e2);
				
				}
						
               
				} 
			
		});
		btnAddCart.setBounds(10, 262, 89, 23);
		contentPane.add(btnAddCart);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(151, 45, 110, 20);
		contentPane.add(scrollPane);
		
		JList listsno = new JList();
		scrollPane.setViewportView(listsno);
		listsno.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtshopid.setText(listsno.getSelectedValue().toString());
			}
			
		});
		
		JButton butcard = new JButton("choose my cart");
		butcard.setIcon(new ImageIcon("G:\\Photoes\\images.png"));
		
			
			JButton butcart = new JButton("Browse Cart");
			butcart.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					DefaultListModel model = new DefaultListModel();
					try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
								"root");

						Statement s = con.createStatement();
						String sql = "SELECT shopping_id FROM shopping_cart  ";
						ResultSet rs = s.executeQuery(sql);
						int row = 0;
						while (rs.next())
							// model.addElement(rs.getString("book_title"));
							// model.addElement(rs.getObject(1).toString() +"-"+
							// rs.getObject(2).toString());
							model.addElement(rs.getObject(1).toString());
							
						
						System.out.println(model);
						listsno.setModel(model);
						rs.close();

					} catch (Exception e1) {

						e1.printStackTrace();

					}
				}
					
					
							
							
							
						
						
				
			});
			butcart.setBounds(520, 11, 154, 53);
			contentPane.add(butcart);
			
			JScrollPane scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(153, 94, 101, 20);
			contentPane.add(scrollPane_1);
			
			JList listiid = new JList();
			scrollPane_1.setViewportView(listiid);
			listiid.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {

					txtuserid.setText(listiid.getSelectedValue().toString());
				}
			});
			
			JButton butbuid = new JButton("Browse User id");
			butbuid.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					DefaultListModel model = new DefaultListModel();
					try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
								"root");

						Statement s = con.createStatement();
						String sql = "SELECT customer_id FROM login ";
						ResultSet rs = s.executeQuery(sql);
						int row = 0;
						while (rs.next())
							// model.addElement(rs.getString("book_title"));
							// model.addElement(rs.getObject(1).toString() +"-"+
							// rs.getObject(2).toString());
							model.addElement(rs.getObject(1).toString()); 
						
						System.out.println(model);
						listiid.setModel(model);
						rs.close();

					} catch (Exception e1) {

						e1.printStackTrace();

					}
				}
				
			});
			butbuid.setBounds(520, 57, 154, 54);
			contentPane.add(butbuid);
			
			JScrollPane scrollPane_2 = new JScrollPane();
			scrollPane_2.setBounds(151, 125, 104, 20);
			contentPane.add(scrollPane_2);
			
			JList listbookid = new JList();
			scrollPane_2.setViewportView(listbookid);
			listbookid.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					txtbookid.setText(listbookid.getSelectedValue().toString());
				}
			});
			
			JButton butbid = new JButton("Browse Book ID");
			butbid.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					DefaultListModel model = new DefaultListModel();
					try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
								"root");

						Statement s = con.createStatement();
						String sql = "SELECT book_id FROM book_details ";
						ResultSet rs = s.executeQuery(sql);
						int row = 0;
						while (rs.next())
							// model.addElement(rs.getString("book_title"));
							// model.addElement(rs.getObject(1).toString() +"-"+
							// rs.getObject(2).toString());
							model.addElement(rs.getObject(1).toString()); 
						
						System.out.println(model);
					listbookid.setModel(model);
						rs.close();

					} catch (Exception e1) {

						e1.printStackTrace();

					}
				}
				
				
			});
			butbid.setBounds(520, 114, 154, 31);
			contentPane.add(butbid);
			
			JScrollPane scrollPane_4 = new JScrollPane();
			scrollPane_4.setBounds(384, 190, 290, 219);
			contentPane.add(scrollPane_4);
			
			JScrollPane scrollPane_3 = new JScrollPane();
			scrollPane_4.setViewportView(scrollPane_3);
			
			table = new JTable();
			scrollPane_3.setViewportView(table);
			
			JButton butsuggest = new JButton("List avilable cards");
			butsuggest.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
								"root");

						String query = "SELECT * FROM shopping_cart ";
						PreparedStatement ps = con.prepareStatement(query);

						ResultSet rs = ps.executeQuery();

						table.setModel(DbUtils.resultSetToTableModel(rs));
					} catch (Exception e2) {

						System.out.println(e2);

					}

				}
				
				
			});
			butsuggest.setBounds(520, 143, 154, 23);
			contentPane.add(butsuggest);
			
			JButton btnNewButton = new JButton("Refresh");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					txtbookid.setText(null);
					txtuserid.setText(null);
					txtpquantity.setText(null);
					txtshopid.setText(null);
				}
			});
			btnNewButton.setBounds(10, 332, 89, 23);
			contentPane.add(btnNewButton);
			
			
			
			

		}
	}
	

