package logandjdbc.ni.lk;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Transparency;

import javax.swing.JComboBox;
import javax.swing.JTable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import javax.swing.border.LineBorder;

import com.mysql.cj.exceptions.RSAException;

import net.proteanit.sql.DbUtils;

import javax.swing.UIManager;
import javax.swing.JScrollBar;
import javax.swing.JCheckBox;
import javax.swing.ImageIcon;

public class BookDetails extends JFrame {

	private JPanel contentPane;
	private JTextField txtbookid;
	private JTextField txtisbn;
	private JTextField txtauthor;
	private JTextField txtprice;
	private JTable table;
	private JTextField txtquant;
	private JTextField txtpubli;
	private JTextField txttitle;
	private JTable tb;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BookDetails frame = new BookDetails();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BookDetails() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 596, 476);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(250, 250, 210));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Book Details");
		lblNewLabel.setForeground(new Color(102, 205, 170));
		lblNewLabel.setFont(new Font("Consolas", Font.BOLD, 31));
		lblNewLabel.setBounds(145, 11, 220, 30);
		contentPane.add(lblNewLabel);

		JLabel lbltitle = new JLabel("Title");
		lbltitle.setFont(new Font("Abadi MT Condensed", Font.BOLD, 16));
		lbltitle.setForeground(new Color(192, 192, 192));
		lbltitle.setBounds(22, 86, 57, 17);
		contentPane.add(lbltitle);

		JLabel lblisbn = new JLabel("ISBN");
		lblisbn.setFont(new Font("Abadi MT Condensed", Font.BOLD, 16));
		lblisbn.setForeground(new Color(192, 192, 192));
		lblisbn.setBounds(22, 139, 46, 14);
		contentPane.add(lblisbn);

		JLabel lblauthor = new JLabel("Author");
		lblauthor.setFont(new Font("Abadi MT Condensed", Font.BOLD, 16));
		lblauthor.setForeground(new Color(192, 192, 192));
		lblauthor.setBounds(22, 187, 46, 14);
		contentPane.add(lblauthor);

		JLabel lblprice = new JLabel("Price");
		lblprice.setFont(new Font("Abadi MT Condensed", Font.BOLD, 16));
		lblprice.setForeground(new Color(192, 192, 192));
		lblprice.setBounds(22, 233, 46, 14);
		contentPane.add(lblprice);

		JLabel lblbookid = new JLabel("Book ID");
		lblbookid.setFont(new Font("Abadi MT Condensed", Font.BOLD, 16));
		lblbookid.setForeground(new Color(192, 192, 192));
		lblbookid.setBounds(22, 51, 77, 17);
		contentPane.add(lblbookid);

		txtbookid = new JTextField();
		txtbookid.setForeground(new Color(160, 82, 45));
		txtbookid.setFont(new Font("Tahoma", Font.BOLD, 13));
		txtbookid.setBackground(new Color(0, 0, 0));
		txtbookid.setBounds(161, 52, 86, 20);
		contentPane.add(txtbookid);
		txtbookid.setColumns(10);

		txtisbn = new JTextField();
		txtisbn.setForeground(new Color(160, 82, 45));
		txtisbn.setFont(new Font("Tahoma", Font.BOLD, 13));
		txtisbn.setBackground(new Color(0, 0, 0));
		txtisbn.setBounds(161, 133, 86, 20);
		contentPane.add(txtisbn);
		txtisbn.setColumns(10);

		txtauthor = new JTextField();
		txtauthor.setForeground(new Color(160, 82, 45));
		txtauthor.setFont(new Font("Tahoma", Font.BOLD, 13));
		txtauthor.setBackground(new Color(0, 0, 0));
		txtauthor.setBounds(161, 187, 86, 20);
		contentPane.add(txtauthor);
		txtauthor.setColumns(10);

		txtprice = new JTextField();
		txtprice.setForeground(new Color(160, 82, 45));
		txtprice.setFont(new Font("Tahoma", Font.BOLD, 13));
		txtprice.setBackground(new Color(0, 0, 0));
		txtprice.setBounds(161, 230, 86, 20);
		contentPane.add(txtprice);
		txtprice.setColumns(10);

		JButton butview = new JButton("View");
		butview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					String query = "SELECT * FROM book_details where book_id= ? ";

					PreparedStatement ps = con.prepareStatement(query);
					ps.setString(1, txtbookid.getText());

					ResultSet rs = ps.executeQuery();

					tb.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception e2) {

					System.out.println(e2);

				}

			}
		});
		butview.setBounds(449, 35, 89, 23);
		contentPane.add(butview);

		JButton butnext = new JButton("Next");
		butnext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerPurchase jframe = new CustomerPurchase();
				jframe.setVisible(true);
				JOptionPane.showMessageDialog(jframe, "success");
			}
		});
		butnext.setBounds(361, 52, 89, 23);
		contentPane.add(butnext);

		JButton butexit = new JButton("Exit");
		butexit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		butexit.setBounds(449, 52, 89, 23);
		contentPane.add(butexit);

		JButton butins = new JButton("Insert");
		butins.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					Statement stmt = con.createStatement();
					String book_id = txtbookid.getText();
					String book_title = txttitle.getText();
					String publisher = txtpubli.getText();
					String ISBN = txtisbn.getText();
					String author = txtauthor.getText();
					String price = txtprice.getText();
					String quantity = txtquant.getText();

					String insert = " INSERT INTO book_details values('" + book_id + "','" + book_title + "','"
							+ publisher + "','" + ISBN + "','" + author + "','" + price + "','" + quantity + "')";
					stmt.executeUpdate(insert);

				} catch (Exception e2) {

				}

			}

			// String[] titles = new String[] {"MY SQL", "Python", "java", "application
			// development", "project management"};
			// JComboBox<String> comboTitle = new JComboBox<String>(titles);

		});
		butins.setBounds(10, 377, 89, 23);
		contentPane.add(butins);

		JLabel lblNewLabel_1 = new JLabel("quantity");
		lblNewLabel_1.setFont(new Font("Abadi MT Condensed", Font.BOLD, 16));
		lblNewLabel_1.setForeground(new Color(192, 192, 192));
		lblNewLabel_1.setBounds(22, 273, 77, 14);
		contentPane.add(lblNewLabel_1);

		txtquant = new JTextField();
		txtquant.setFont(new Font("Tahoma", Font.BOLD, 13));
		txtquant.setForeground(new Color(160, 82, 45));
		txtquant.setBackground(new Color(0, 0, 0));
		txtquant.setBounds(161, 267, 86, 20);
		contentPane.add(txtquant);
		txtquant.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("publisher");
		lblNewLabel_2.setFont(new Font("Abadi MT Condensed", Font.BOLD, 16));
		lblNewLabel_2.setForeground(new Color(192, 192, 192));
		lblNewLabel_2.setBounds(22, 315, 77, 17);
		contentPane.add(lblNewLabel_2);

		txtpubli = new JTextField();
		txtpubli.setFont(new Font("Tahoma", Font.BOLD, 13));
		txtpubli.setForeground(new Color(160, 82, 45));
		txtpubli.setBackground(new Color(0, 0, 0));
		txtpubli.setBounds(161, 312, 86, 20);
		contentPane.add(txtpubli);
		txtpubli.setColumns(10);

		txttitle = new JTextField();
		txttitle.setForeground(new Color(160, 82, 45));
		txttitle.setFont(new Font("Tahoma", Font.BOLD, 13));
		txttitle.setBackground(new Color(0, 0, 0));
		txttitle.setBounds(161, 83, 86, 20);
		contentPane.add(txttitle);
		txttitle.setColumns(10);

		JButton butupdate = new JButton("Update");
		butupdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				  try { Class.forName("com.mysql.jdbc.Driver"); Connection con =
				  DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
				  "root");
				  
				  Statement stmt = con.createStatement();
				  String book_id=txtbookid.getText();
				  String book_title=txttitle.getText();
				  String publisher =txtpubli.getText();
				  String ISBN=txtisbn.getText(); 
				  String author=txtauthor.getText(); 
				  String price=txtprice.getText();
				  String quantity=txtquant.getText();
				  
				  
				  
				  
				 
				 
				 String update ="UPDATE book_details SET book_title =? WHERE book_id=?" ;
				  
				 stmt.executeUpdate(update);
				  
				 
				  } catch (Exception e2) {
				  
				 }
				 
				/*
				 * try{ Class.forName("com.mysql.jdbc.Driver"); Connection con =
				 * DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
				 * "root");
				 * 
				 * 
				 * String query ="update book_details set book_title= ? where book_id= ?";
				 * PreparedStatement ps =con.prepareStatement(query); ps.setString(1,
				 * txtbookid.getText()); ps.setString(2, txttitle.getText()); ResultSet rs =
				 * ps.executeQuery();
				 * 
				 * 
				 * 
				 * 
				 * 
				 * 
				 * tb.setModel(DbUtils.resultSetToTableModel(rs)); } catch (Exception e2) {
				 * 
				 * System.out.println(e2);
				 * 
				 * }
				 */

				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");
					Statement statement = con.createStatement();

					String update = " update book_details set book_title= ? where book_id= ? ";
					PreparedStatement ps = con.prepareStatement(update);
					ps.setString(1, txtbookid.getText());
					ps.setString(2, txttitle.getText());
					/*
					 * ps.setString(3, txtpubli.getText()); ps.setString(4, txtisbn.getText());
					 * ps.setString(5, txtauthor.getText()); ps.setString(6, txtquant.getText());
					 */
					ps.execute();
					JOptionPane.showMessageDialog(null, " success fully updated the selected book detail");

					// tb.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e2) {

					System.out.println(e2);

				}

			}
		});
		butupdate.setBounds(10, 403, 89, 23);
		contentPane.add(butupdate);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(257, 151, 304, 237);
		contentPane.add(scrollPane_1);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane_1.setViewportView(scrollPane);

		tb = new JTable();
		scrollPane.setViewportView(tb);

		JButton butviall = new JButton("view all");
		butviall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					String query = "SELECT * FROM book_details ";
					PreparedStatement ps = con.prepareStatement(query);

					ResultSet rs = ps.executeQuery();

					tb.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e2) {

					System.out.println(e2);

				}

			}
		});
		butviall.setBounds(361, 35, 89, 23);
		contentPane.add(butviall);

		JButton butdel = new JButton("Delete Books");
		butdel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");
					Statement statement = con.createStatement();

					String delete = "delete from book_details where book_id= ? ";
					PreparedStatement ps = con.prepareStatement(delete);
					ps.setString(1, txtbookid.getText());
					// ps.setString(2, txttitle.getText());
					/*
					 * ps.setString(3, txtpubli.getText()); ps.setString(4, txtisbn.getText());
					 * ps.setString(5, txtauthor.getText()); ps.setString(6, txtquant.getText());
					 */
					ps.execute();
					JOptionPane.showMessageDialog(null, " success fully deleted the selected book detail");

					// tb.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, " success fully deleted");
					System.out.println(e2);

				}
			}
		});
		butdel.setBounds(110, 377, 86, 21);
		contentPane.add(butdel);

		JCheckBox chkconform = new JCheckBox("agree as an admin");
		chkconform.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (chkconform.isSelected()) {

					butins.setEnabled(true);
					JOptionPane.showMessageDialog(null, "  WE are confirming the privilage settings");
					// MainLogin jframe = new MainLogin();
					// jframe.setVisible(true);

				} else {
					butins.setEnabled(false);
					// JOptionPane.showMessageDialog(null, " warning!!! you have not the
					// privillage");

				}

			}
		});
		chkconform.setBounds(46, 347, 139, 23);
		contentPane.add(chkconform);

		JButton butrefresh = new JButton("Refresh");
		butrefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtbookid.setText(null);
				txttitle.setText(null);
				txtauthor.setText(null);
				txtpubli.setText(null);
				txtprice.setText(null);
				txtquant.setText(null);
				txtisbn.setText(null);

			}
		});
		butrefresh.setBounds(410, 84, 89, 18);
		contentPane.add(butrefresh);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setIcon(new ImageIcon("G:\\Photoes\\4255186-digital-wallpaper.jpg"));
		lblNewLabel_3.setBounds(-142, 0, 690, 477);
		contentPane.add(lblNewLabel_3);

	}
}