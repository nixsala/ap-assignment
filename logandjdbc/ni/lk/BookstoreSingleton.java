package logandjdbc.ni.lk;

import java.sql.Connection;
import java.sql.DriverManager;

public class BookstoreSingleton {
	private static BookstoreSingleton bookstoreSingleton = new BookstoreSingleton();

	private BookstoreSingleton() {
	}

	public static BookstoreSingleton getInstance() {
		return bookstoreSingleton;
	}

	public void dbConnection() {
		Connection con;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root", "root");

		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
