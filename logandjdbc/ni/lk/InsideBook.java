package logandjdbc.ni.lk;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import java.awt.Font;

public class InsideBook extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InsideBook frame = new InsideBook();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InsideBook() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 603, 343);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbldisplay = new JLabel("New label");
		lbldisplay.setIcon(new ImageIcon("G:\\Photoes\\5477bbae908be447362b7d2414328a5d.png"));
		lbldisplay.setBounds(0, 38, 349, 178);
		contentPane.add(lbldisplay);
		
		JButton butsearch = new JButton("Search Books");
		butsearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerSelectBook jframe = new CustomerSelectBook();
		          jframe.setVisible(true);
			}
		});
		butsearch.setBounds(0, 238, 118, 37);
		contentPane.add(butsearch);
		
		JButton butexit = new JButton("Exit");
		butexit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		butexit.setBounds(321, 238, 89, 37);
		contentPane.add(butexit);
		
		JLabel lblhead = new JLabel("Book store");
		lblhead.setFont(new Font("Abadi MT Condensed", Font.BOLD, 23));
		lblhead.setForeground(new Color(240, 230, 140));
		lblhead.setBounds(160, 11, 141, 16);
		contentPane.add(lblhead);
		
		JButton butprevi = new JButton("previous");
		butprevi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Login jframe = new Login();
		          jframe.setVisible(true);
			}
		});
		butprevi.setBounds(174, 236, 89, 41);
		contentPane.add(butprevi);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BookDetails jframe = new BookDetails();
				jframe.setVisible(true);

			}
		});
		btnNewButton.setIcon(new ImageIcon("G:\\Photoes\\images (6).jpg"));
		btnNewButton.setBounds(375, 11, 202, 186);
		contentPane.add(btnNewButton);
	}
}
