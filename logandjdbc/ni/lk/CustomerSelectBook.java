package logandjdbc.ni.lk;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.mysql.cj.exceptions.RSAException;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CustomerSelectBook extends JFrame {

	private JPanel contentPane;
	private JTable tballbooks;
	private JTextField txttitle;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerSelectBook frame = new CustomerSelectBook();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CustomerSelectBook() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 900);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(245, 245, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Customer ");
		lblNewLabel_1.setBackground(new Color(255, 228, 181));
		lblNewLabel_1.setFont(new Font("Gill Sans Condensed", Font.BOLD, 20));
		lblNewLabel_1.setForeground(new Color(0, 0, 205));
		lblNewLabel_1.setBounds(199, 21, 225, 97);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("G:\\Photoes\\download.jpg"));
		lblNewLabel.setBounds(0, 0, 189, 144);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_2 = new JLabel("Choose Book Title");
		lblNewLabel_2.setFont(new Font("Aachen BT", Font.BOLD, 15));
		lblNewLabel_2.setForeground(new Color(128, 128, 128));
		lblNewLabel_2.setBounds(20, 180, 233, 20);
		contentPane.add(lblNewLabel_2);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(10, 271, 310, 179);
		contentPane.add(scrollPane_2);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane_2.setViewportView(scrollPane);

		tballbooks = new JTable();
		scrollPane.setViewportView(tballbooks);

		JButton butviewallbook = new JButton("View all books");
		butviewallbook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					String query = "SELECT * FROM book_details ";

					PreparedStatement ps = con.prepareStatement(query);
					// ps.setString(1, txtid.getText());

					ResultSet rs = ps.executeQuery();

					tballbooks.setModel(DbUtils.resultSetToTableModel(rs));
					// txtbtitle.setText(rs.getString("book_title"));

				} catch (Exception e2) {

					System.out.println(e2);

				}

			}
		});
		butviewallbook.setIcon(null);
		butviewallbook.setBounds(422, 392, 125, 35);
		contentPane.add(butviewallbook);

		JButton butonebyone = new JButton("choose book");
		butonebyone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					String query = "SELECT * FROM book_details where book_title =  ? ";

					PreparedStatement ps = con.prepareStatement(query);
					ps.setString(1, txttitle.getText());

					ResultSet rs = ps.executeQuery();
					// txtbtitle.setText(rs.getString("book_title"));

					// txtbtitle.setText(rs.getString("book_title"));
					
					tballbooks.setModel(DbUtils.resultSetToTableModel(rs));
					

				} catch (Exception e2) {

					System.out.println(e2);

				}

			}

		});
		butonebyone.setBounds(426, 301, 121, 35);
		contentPane.add(butonebyone);

		JLabel lblNewLabel_5 = new JLabel("New label");
		lblNewLabel_5.setIcon(new ImageIcon("G:\\Photoes\\azure_3e58e70955622b115c9d4a4401c0d943.gif"));
		lblNewLabel_5.setBounds(313, -5, 444, 154);
		contentPane.add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel("Selection");
		lblNewLabel_6.setForeground(new Color(255, 215, 0));
		lblNewLabel_6.setFont(new Font("Gill Sans Condensed", Font.BOLD, 20));
		lblNewLabel_6.setBounds(219, 90, 101, 35);
		contentPane.add(lblNewLabel_6);

		txttitle = new JTextField();
		txttitle.setBounds(488, 182, 86, 20);
		contentPane.add(txttitle);
		txttitle.setColumns(10);
		
		JButton butnext = new JButton("Next");
		butnext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ShoppingCart iframe = new ShoppingCart();
				iframe.setVisible(true);
			}
		});
		butnext.setBounds(52, 517, 95, 35);
		contentPane.add(butnext);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(280, 184, 144, 35);
		contentPane.add(scrollPane_1);
		
		JList list = new JList();
		scrollPane_1.setViewportView(list);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txttitle.setText(list.getSelectedValue().toString());

			}
		});
		
		JButton butchoose = new JButton("Start Selection");
		butchoose.setFont(new Font("Verdana", Font.BOLD, 12));
		butchoose.setForeground(new Color(160, 82, 45));
		butchoose.setBackground(new Color(245, 245, 245));
		butchoose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					DefaultListModel model = new DefaultListModel();

					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store", "root",
							"root");

					Statement s = con.createStatement();
					String sql = "SELECT book_title FROM book_details  ";
					ResultSet rs = s.executeQuery(sql);
					int row = 0;
					while (rs.next())
						// model.addElement(rs.getString("book_title"));
						// model.addElement(rs.getObject(1).toString() +"-"+
						// rs.getObject(2).toString());
						model.addElement(rs.getObject(1).toString());

					System.out.println(model);
					list.setModel(model);
					rs.close();

				} catch (Exception e1) {

					e1.printStackTrace();

				}
			}
			
		});
		butchoose.setBounds(748, 0, 136, 144);
		contentPane.add(butchoose);
		
		JButton btnNewButton = new JButton("Exit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton.setBounds(185, 518, 86, 32);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Clear");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txttitle.setText(null);
				tballbooks.setModel(null);
			}
		});
		btnNewButton_1.setBounds(313, 520, 101, 29);
		contentPane.add(btnNewButton_1);
	}
}
