package admin.ni;

public final class User {

	
		  
	   
	    private int id; 
	    private String name; 
	    private String address; 
	  
	   
	    public User  setId(int id) 
	    { 
	        this.id = id; 
	        return this; 
	    } 
	  
	    public User setName(String name) 
	    { 
	        this.name = name; 
	        return this; 
	    } 
	  
	    public User setAddress(String address) 
	    { 
	        this.address = address; 
	        return this; 
	    } 
	  
	    @Override
	    public String toString() 
	    { 
	        return "id = " + this.id + ", name = " + this.name +  
	                               ", address = " + this.address; 
	    } 
	} 
