package books.ni;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FacadePatternCustomer {
	private static int  choice;  
    public static void main(String args[]) throws NumberFormatException, IOException{  
        do{       
            System.out.print("========= Book Store ============ \n");  
            System.out.print("            1. java.              \n");  
            System.out.print("            2. bussinesmangement.              \n");  
            System.out.print("            3. database.            \n");  
            System.out.print("            4. Exit.                     \n");  
            System.out.print("Enter your choice: ");  
              
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));  
            choice=Integer.parseInt(br.readLine());  
            ShoppingCart sk=new ShoppingCart();  
              
            switch (choice) {  
            case 1:  
                {   
                  sk.datbaseContent();
                    }  
                break;  
       case 2:  
                {  
                  sk.javaContent();        
                    }  
                break;    
       case 3:  
                           {  
                           sk.businessmanagementContent();     
                           }  
                   break;     
            default:  
            {    
                System.out.println("Nothing You purchased");  
            }         
                return;  
            }  
              
      }while(choice!=4);  
   }  
}
