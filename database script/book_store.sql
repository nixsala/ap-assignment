-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.15-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for book_store
CREATE DATABASE IF NOT EXISTS `book_store` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `book_store`;


-- Dumping structure for table book_store.admin_login
CREATE TABLE IF NOT EXISTS `admin_login` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(100) DEFAULT NULL,
  `admin_password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table book_store.admin_login: ~3 rows (approximately)
/*!40000 ALTER TABLE `admin_login` DISABLE KEYS */;
INSERT INTO `admin_login` (`admin_id`, `admin_name`, `admin_password`) VALUES
	(1, 'head admin', 'headadmin'),
	(2, 'supervisor', 'super123'),
	(3, 'technical officer', 'tech123');
/*!40000 ALTER TABLE `admin_login` ENABLE KEYS */;


-- Dumping structure for table book_store.book_details
CREATE TABLE IF NOT EXISTS `book_details` (
  `book_id` int(10) NOT NULL,
  `book_title` varchar(70) NOT NULL,
  `publisher` varchar(70) NOT NULL,
  `ISBN` text NOT NULL,
  `author` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table book_store.book_details: ~16 rows (approximately)
/*!40000 ALTER TABLE `book_details` DISABLE KEYS */;
INSERT INTO `book_details` (`book_id`, `book_title`, `publisher`, `ISBN`, `author`, `price`, `quantity`) VALUES
	(1001, 'web designing', 'mtc comany', '7895636542659', 'Hook', 1200, 100),
	(1002, 'java programming', 'abc company', '4578963256412', 'Reeves', 800, 200),
	(1003, 'bussiness management', 'co and finace', '6598325641235', 'Helana', 400, 400),
	(1004, 'python lanaguage', 'threan industry', '4569876326954', 'Mandela', 1000, 150),
	(1006, 'Artificial_inrelligent', 'world mart company', '9635698756424', 'Bernold', 3000, 350),
	(1007, 'Machine Language', 'cambridge', '456897563215691', 'George', 2110, 250),
	(1008, 'Hacking', 'Edman ', '4578263246312', 'Merin', 250, 12),
	(1010, 'Mathamatical and statics', 'Dollar brand', '56987891236', 'gelax', 1050, 45),
	(1011, 'English Literature', 'Diamond Company', '9012456756424', 'Oliver', 550.25, 12),
	(1015, 'Anatomy', 'queen and sperk', '123654789653', 'Terman', 1500, 25),
	(1031, 'Technology', 'EDEX', '12365986356', 'Darwin', 990, 1000),
	(1041, 'multi media', 'joakl', '458963217', 'Honey', 120, 10000),
	(1060, 'Molecular biology', 'loetenkan', '45789653256', 'Tennison', 330, 10),
	(1084, 'Merchant of Venice', 'yedman', '457896533216', 'Anita Desai', 1500, 500),
	(1090, 'Social science', 'moulanay', '569832165478', 'Anita Desai', 450, 500),
	(1097, 'Heamatology', 'thenman', '789654236985', 'Dickeran', 650, 100);
/*!40000 ALTER TABLE `book_details` ENABLE KEYS */;


-- Dumping structure for table book_store.book_purchase
CREATE TABLE IF NOT EXISTS `book_purchase` (
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `purchase_day_time` datetime NOT NULL,
  `details` varchar(150) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table book_store.book_purchase: ~0 rows (approximately)
/*!40000 ALTER TABLE `book_purchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `book_purchase` ENABLE KEYS */;


-- Dumping structure for table book_store.login
CREATE TABLE IF NOT EXISTS `login` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(30) DEFAULT NULL,
  `customer_password` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- Dumping data for table book_store.login: ~20 rows (approximately)
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` (`customer_id`, `customer_name`, `customer_password`) VALUES
	(1, 'Amuthini', '456983wfgrli'),
	(2, 'Thifiya', 'fatalerror45789'),
	(3, 'Lily', 'skyland23'),
	(4, 'Meera', 'thedark45789years'),
	(5, 'Saranga', 'thurtyone4567dot'),
	(6, 'Subani', 'Malware123456'),
	(7, 'Ganimoli', 'studywell'),
	(8, 'Lavanya', 'goodmoringmsg23'),
	(9, 'Narmatha', 'get456well'),
	(10, 'Abirami', 'the45bus45road'),
	(11, 'Admin', 'admin123'),
	(12, 'abi', 'hii'),
	(13, 'Kamala', 'kamal'),
	(14, 'Vimala', 'vimala'),
	(15, 'Sushma', 'sushma'),
	(17, 'shalomy', 'shalomy123'),
	(18, 'kavitha', 'kavitha18'),
	(22, 'Hena', 'hene'),
	(31, 'Manila', 'glass'),
	(55, 'Nitharsha', 'nithar');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;


-- Dumping structure for table book_store.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `log_id` int(11) DEFAULT NULL,
  `log_details` varchar(200) DEFAULT NULL,
  `create_date_time` datetime DEFAULT NULL,
  `create_by` varchar(100) DEFAULT NULL,
  `modify_date_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table book_store.logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;


-- Dumping structure for table book_store.main_login
CREATE TABLE IF NOT EXISTS `main_login` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pw` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table book_store.main_login: ~2 rows (approximately)
/*!40000 ALTER TABLE `main_login` DISABLE KEYS */;
INSERT INTO `main_login` (`id`, `name`, `pw`) VALUES
	(11, 'admin', 'admin123'),
	(12, 'customer', 'customer123');
/*!40000 ALTER TABLE `main_login` ENABLE KEYS */;


-- Dumping structure for table book_store.shopping_cart
CREATE TABLE IF NOT EXISTS `shopping_cart` (
  `shopping_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `quantity_of_purchase` int(11) NOT NULL,
  PRIMARY KEY (`shopping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table book_store.shopping_cart: ~5 rows (approximately)
/*!40000 ALTER TABLE `shopping_cart` DISABLE KEYS */;
INSERT INTO `shopping_cart` (`shopping_id`, `user_id`, `book_id`, `quantity_of_purchase`) VALUES
	(111011, 1008, 6, 4),
	(111102, 1, 1005, 3),
	(111103, 12, 1009, 5),
	(111104, 1010, 1, 2),
	(111108, 1011, 7, 10);
/*!40000 ALTER TABLE `shopping_cart` ENABLE KEYS */;


-- Dumping structure for table book_store.user_details
CREATE TABLE IF NOT EXISTS `user_details` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_password` varchar(100) DEFAULT NULL,
  `user_address` varchar(200) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table book_store.user_details: ~11 rows (approximately)
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` (`user_id`, `user_name`, `user_password`, `user_address`, `status`) VALUES
	(1, 'Amuthini', '456983wfgrli', 'Colombo', 'Actiive'),
	(2, 'Thifiya', 'fatalerror45789', 'Kilinochi', 'Processing'),
	(3, 'Lily', 'skyland23', 'Jaffna_nallur', 'In_active'),
	(4, 'Meera', 'thedark45789years', 'Matara', 'Active'),
	(5, 'Saranga', 'theturtyone4567dot', 'Galle', 'In_active'),
	(6, 'Subani', 'Malware123456', 'Trincomali', 'Processing'),
	(7, 'Ganimoli', 'studywell', 'Jaffna_chunnakam', 'Active'),
	(8, 'Lavanya', 'goodmorningmasg123', 'Bamdarawalla', 'Processing'),
	(9, 'Narmatha', 'get456well', 'Manner', 'In_active'),
	(10, 'Abirami', 'the45bus45road', 'Kegala', 'Active'),
	(11, 'Admin', 'admin123', 'jaffna_branch', 'Active');
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
