package builder.ni;

public class Run{
 

    public static void main(String[] args)
    {
    	MecahnicWorker engineer1 = new MecahnicWorker();
    	ServiceWorker engineer2= new ServiceWorker();
        CarBuilder carBuilder1 = new CarBuilder(engineer1);
        CarBuilder carBuilder2 = new CarBuilder(engineer2);

        carBuilder1.buildACar();
        carBuilder2.buildACar();

        Car car1 = carBuilder1.getCar();
        Car car2 = carBuilder2.getCar();

        System.out.println("Two brands of cars were made by using the Builder design pattern:");
        System.out.println(car1.toString());
        System.out.println(car2.toString());
    }
}