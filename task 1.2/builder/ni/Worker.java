package builder.ni;

public interface Worker {
    public void bulidEngine();
    public void buildGear();
    public void assembleWheels();
    public void paintTheCar();
    public Car getCar();
}